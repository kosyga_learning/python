def laba2(number, stepan):
    digits = '0123456789abcdefghijklmnopqrstuvwxyz'
    if stepan > len(digits): return None
    text = ""
    while number > 0:
        text += digits[number % stepan]
        number //= stepan
    return text[::-1]
print("Введите число")
a = int(input())
print("Введите степень")
b = int(input())
print(laba2(a,b))