import os.path

class Person:
    values = dict()
    
    def __init__(self, values:dict = {'Фамилия:' : '', 'Имя:': '', 'Отчество:' : '', 'Телефон:' : '', 'E-mail:': '', 'Адрес:': '', 'Компания:': '', 'Должность:': ''}):
        self.values = values

    def getFio(self):
        return list(self.values.values())[:3]
    
    def getData(self):
        return self.values

class PList:
    List = list()
    filename = ""
    def __init__(self) -> None:
        pass
    
    def deserialize(self, filename):
        if not filename: filename = "book.txt"

        values = Person().getData()
        
        def newPers(self, values:dict):
            if not all(list(values.values())[:5]):
                if any(list(values.values())):
                    print("not enought data")
                return
            self.List.append(Person(values))
            
        if(os.path.exists(filename)):
            self.filename = filename
            with open(filename, encoding = 'utf-8', mode = "r") as fin:
                for line in fin:
                    words = line.split()
                    
                    if not words:
                        newPers(self, values)
                        values = values.fromkeys(values, "")

                    else:
                        if words[0] in list(values.keys()):
                            values[words[0]] = ' '.join(words[1:])
                        else:
                            print("unknown key")
                newPers(self, values)
                print("Файл загружен!")
        else: print("Такого файла нет")

    def showAll(self):
        l = dict()
        for i in range(0, len(self.List)):
            l[i+1] = ' '.join(self.List[i].getFio())
        l = dict(sorted(l.items(),key = lambda item: item[1]))
        print(l)
    
    def findNum(self, num):
        if len(self.List) >= num-1:
            print(self.List[num-1].getData())
        else: print("Такого пользователя нет")
                
    def findFio(self, Fio:str):
        data = dict()
        for pers in self.List:
            if Fio.split() == pers.getFio():
                data = pers.getData()
        if not data: print("Такого пользователя нет")
        else: print(data)

    def newPers(self):
        values = Person().getData()
        for value in values:
            values[value] = str(input(value+" "))
        if not all(list(values.values())[:5]):
                print("not enought data")
                return
        return values

    def edit(self, num):
        if not self.filename:
            print("Нечего редактировать")
            return
        self.List[num+1] = Person(self.newPers())
        self.updateBase()

    def updateBase(self):
        text = ""
        for pers in self.List:
            for key, value in pers.getData().items():
                text += key + " " + value + "\n"
            text += "\n"
        if not self.filename: self.filename = str(input("Введите название файла для сохранения: "))
        with open(self.filename, encoding = 'utf-8', mode = "w") as fout:
            fout.write(text)

    def menu(self):
        print("Записная книжка \n Вы можете: \n\t1 - Загрузить из файла. \n\t2 - Вывести все ФИО \n\t3 - Найти пользователя по номеру записи \n\t4 - Найти пользователя по ФИО \n\t5 - Создать новую запись \n\t6 - Редактировать запись")
        task = int(input("Введите номер действия: "))
        if task == 1: self.deserialize(str(input("Введите название файла: ")))
        elif task == 2: self.showAll()
        elif task == 3: self.findNum(int(input("Введите номер записи: ")))
        elif task == 4: self.findFio(str(input("Введите ФИО через пробел: ")))
        elif task == 5:
            self.List.append(Person(self.newPers()))
            self.updateBase()   
        elif task == 6: self.edit(int(input("Введите номер записи: ")))
        else: print("Такого действия нет")
        print('\n'+"////////////////////////////"+'\n')
        self.menu()

List = PList()
List.menu()