def equ(sl1, sl2, maxk):
    k = 0
    if len(sl1)!= len(sl2): return False
    for i in range(0, len(sl1)):
        if sl1[i] != sl2[i]: k+=1
        if k > maxk: return False
    return True

def getBase(filename):
    base = []
    with open(filename) as fin:
        for line in fin:
            base.append(line.replace("\n",""))
    return base

def orfo(filename, d):
    text = ""
    with open(filename, encoding = 'utf-8', mode = "r") as fin:
        st = 0
        for line in fin:
            st+=1
            slova = line.replace("-", " ").split()
            for slovo in slova:
                test = slovo.replace(",","").replace(".","").replace("!","").replace("?","").lower()
                if(test not in d):
                    res = []
                    for elem in d:
                        if equ(test,elem,1):
                            res.append(elem)
                    text += slovo + '\n' + '\t' + '\t'
                    print("Номер строки: " + str(st) + '\n' + "Похожие слова: ")
                    print(res)
                else:
                    text += slovo + " "
            text += '\n'

    with open(filename, encoding = 'utf-8', mode = "w") as fout:
        fout.write(text)

d = getBase("RUS.txt")
orfo("text.txt", d)