def saw(array):
    maxa = 0
    a = 2
    prev = array[0] > array[1]
    for i in range(1,len(array)-1):
        if array[i] != array[i+1]:
            ubilo = array[i] > array[i+1]
            if prev != ubilo:
                a+=1
            else:
                maxa = max(a,maxa) 
                a = 1
            prev = ubilo
        else:
            maxa = max(a,maxa) 
            a = 0  
        maxa = max(a,maxa) 
    return maxa
arr = list(map(int, input().split()))
print(saw(arr))