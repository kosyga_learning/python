import math
def deviders(num):
    array = list()
    for i in range(1,num):
        if  num % i == 0:
            array.append(i)
    return array

def perfect(num):
    array = list()
    for i in range(1,num):
        a = deviders(i)
        if sum(a) == i:
            array.append(i)
    return array

def coub(num):
    a = num * (num - 1) + 1 
    answer = a
    for i in range(1,num):
        a += 2
        answer+=a;
    return answer

def fact(num):
    i = 0
    while num != 0:
        num//=10
        i+=1 
    return math.factorial(i)

def razm(k,n):
    if k>n: 
        k, n = n, k
    s = 1
    for i in range(n,n-k,-1):
        s*=i
    return s


def soch(k,n):
    s=1
    k = k if k < n-k else n-k
    for i in range(1,k+1):
        s*=i
    return razm(k,n)//s

print("Введите номер задания")
z = int(input())
if z == 1:
    f = int(input())
    print(deviders(f))
elif z==2: 
    f = int(input())
    print(perfect(f)) 
elif z==3: 
    f = int(input())
    print(coub(f))
elif z==4: 
    f = int(input())
    print(fact(f))
elif z==5: 
    f = int(input())
    h = int(input())
    print(razm(f,h))
elif z==6: 
    f = int(input())
    h = int(input())
    print(soch(f,h))