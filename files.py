def solve(filename):
    text = ""
    with open(filename, "r") as fin:
        for line in fin:
            text += line.strip() + " = " + str(eval(line)) + '\n'
    with open(filename, "w") as fout:
        fout.write(text)

def check(filename):
    text = ""
    with open(filename, "r") as fin:
        for line in fin:
            minS = 36
            values = line.split()
            for i in range(2,36):
                try:
                    a = int(values[0],i); b = int(values[2],i); c = int(values[4],i)
                    if(eval(str(a)+values[1]+str(b))==c):
                        minS = min(minS,i)
                except:
                    pass
            text += line.strip() + " // " + str(minS) + '\n'
    with open(filename, "w") as fout:
        fout.write(text)



def menu():
    task = int(input("Введите номер задания: "))
    if task == 1:
        solve("text.txt")
    elif task == 2:
        check("text.txt")
    else: print("Такого задания нет")
    print('\n'+"////////////////////////////"+'\n')
    menu()

menu()