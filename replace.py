def white(text):
    whitespace = [' ', '\t', '\n']
    i = 1
    while i < len(text):
        if text[i] in whitespace and text[i-1] in whitespace:
            sym = text[i-1] + text[i]
            while sym in text:
                text = text.replace(sym, ' ')
        i+=1
    return text

def slovar(text, digit):
    slova = text.split()
    d = dict()
    for slovo in slova:
        if slovo >= digit:
            if slovo in d:
                d[slovo]+=1
            else:
                d[slovo] = 1
    return d

def eshohushe(d, text):
    slova = text.split()
    for slovo in slova:
        if slovo in d:
            d[slovo]+=1
        else:
            d[slovo] = 1
    return d

def sortedict(d, text):
    d = eshohushe(d,text)

    sd = dict(sorted(d.items(),key = lambda item: item[1], reverse=True)).keys()
    spisok = []
    for key in sd:
        if key in text: spisok.append(key)
    return spisok

def menu():
    task = int(input("Введите номер задания: "))
    if task == 1:
        text = input("Enter text: ")
        print(white(text))
    elif task == 2:
        text = input("Enter text: ")
        digit = input("Enter number: ")
        print(slovar(text, digit))
    elif task == 3:
        text = input("Enter text: ")
        print(eshohushe({"dwad":1}, text))
    elif task == 4:
        text = input("Enter text: ")
        print(sortedict({"dwad":1}, text))
    else: print("Такого задания нет")
    print('\n'+"////////////////////////////"+'\n')
    menu()

menu()
            






