import re

def checkMail(file):
    with open(file, encoding = 'utf-8', mode = "r") as fin:
        emails = fin.read().split("\n")
        for email in emails:
            if re.match('[a-zA-Z]*[a-zA-Z0-9\.\-\_]+@[a-zA-Z]*[a-zA-Z0-9\.\-]\.+[A-Za-z]', email):
                s1,s2 = email.split("@")
                with open(str(s2)+'.txt', 'a+') as fout:
                    fout.write(str(s1) + '\n')

def getTrueNumbers(file):
    with open(file, encoding = 'utf-8', mode = "r") as fin:
        numbers = fin.read().split("\n")
        for number in numbers:
            if (re.match('(\+7|8)\(\d{3}\)[\d\-?]{7}', number)) or (re.match('(\+7|8)\(\d{4}\)[\d\-?]{6}', number)) or (re.match('(\+7|8)\(\d{5}\)[\d\-?]{5}', number)): 
                print(number)

file = "numbers.txt"
#getTrueNumbers(file)
checkMail("emails.txt")