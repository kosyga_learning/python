import os

def depth(catalog):
    return len(list(os.walk(catalog)))-3

def size(catalog):
    total_size = 0
    for file in os.listdir(catalog):
        if os.path.isdir(catalog + "\\" + file):
            total_size += size(catalog + "\\" + file)
        else:
            total_size += os.path.getsize(catalog + "\\" + file)
    return total_size

def space(catalog):
    common = size(catalog)
    for file in os.listdir(catalog):
        if os.path.isdir(catalog + "\\" + file):
            print("Подкаталог " + file + " занимает " + str((size(catalog + "\\" + file) / common)*100) + " % от каталога")

print("Макс глубина: " + str(depth("C:\\Users\\ITAdmin\\Documents\\learning\\second sem\\python\\distr")))
space("C:\\Users\\ITAdmin\\Documents\\learning")