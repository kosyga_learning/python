import os
from pprint import pprint

def distribute(catalog = '', list = list()):
    if os.path.isdir(catalog):
        os.chdir(catalog)
        if list:
            for folder in list:
                if not os.path.isdir(folder):
                    os.mkdir(folder.lower())
        files = os.listdir()
        for file in files:
            if os.path.isfile(file):
                type = file.split(".")[-1]
                if not list and not os.path.isdir(type):
                    os.mkdir(type)
                if os.path.isdir(type):
                    os.replace(catalog + "\\" + file, catalog + "\\" + type + "\\" + file)

def sort_key(e):
    parts = e.split('.')
    return parts[-1] + '.' + ''.join(parts[:-1])

def sort(catalog, sort_type, direction = False):
    files = os.listdir(catalog)
    key = None
    match sort_type:
        case 2:
            key = os.path.getsize
        case 3:
            key = sort_key
    pprint(sorted(files, key = key, reverse = direction))

def menu():
    task = int(input("Введите номер задания: "))
    catalog = input("Введите название папки: ")
    if not catalog: catalog = "G:\libraries\Documents\Learning\second sem\python\distr"

    if task == 1:
        names = input("Введите имена катологов через пробел: ")
        list = None if not names else names.split(' ')
        distribute(catalog, list)

    elif task == 2:
        sort_type = int(input('Сортировки:\n1)По имени\n2)По размеру\n3)Двумерный список\n  Ваш выбор: '))
        print('\n')
        direction = input('Укажите напривление сортировки: (+/-): ').lower().strip() == '+'
        sort(catalog, sort_type, direction)

    else: print("Такого задания нет")
    print('\n'+"////////////////////////////"+'\n')
    menu()

menu()